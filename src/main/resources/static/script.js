window.addEventListener('DOMContentLoaded', run);

function run() {
    console.log("Started...");
    loadStudents(100, 1);
}

async function loadStudents(pageSize, pageNo) {
    const response = await fetch('/studentai');
    const students = await response.json();
    console.log(students);

    showStudents(students);
}

function showStudents(students) {
    let element = document.getElementById("title");
    element.innerHTML = "List of Students"
    let container = document.getElementById("container")
    let table = 'No data'
    if (Array.isArray(students)) {
        table = `
            <table class="table">
                <tr>
                    <th>id</th>
                    <th>First name</th>
                    <th>Last name</th>
                    <th>e-mail</th>
                </tr>
        `

        students.forEach(student => {
            table += `
                <tr >
                    <td onclick="loadStudent(${student.id})">${student.id}</></td>
                    <td>${student.firstName}</td>
                    <td>${student.lastName}</td>
                    <td>${student.email}</td>
                    <td onclick="deleteStudent(${student.id})"><button id="deleteBtn">Delete</button></td>
                    <td onclick="editStudent(${student.id})"><button id="editBtn">Edit</button></td>
                </tr>
            `
        })

        table += '</table>'
    }
    table += '            <button id="createBtn">Create</button>'
    container.innerHTML = table;
    addStudent()
}

async function loadStudent(id) {
    const response = await fetch('/studentai/' + id);
    const student = await response.json();
    console.log(student);

    showStudent(student);
}

function showStudent(student) {
    let element = document.getElementById("title");
    element.innerHTML = "Student #" + student.id

    let studentInfo = `
        <div class="container">
            <div class="row">
                <div class="col-4">id</div>
                <div class="col-8"><strong>${student.id}</strong></div>
            </div>
            <div class="row">
                <div class="col-4">First name</div>
                <div class="col-8"><strong>${student.firstName}</strong></div>
            </div>
            <div class="row">
                <div class="col-4">Last name</div>
                <div class="col-8"><strong>${student.lastName}</strong></div>
            </div>
            <div class="row">
                <div class="col-4">e-mail</div>
                <div class="col-8"><strong>${student.email}</strong></div>
            </div>
        </div>
    `

    if (Array.isArray(student.grades)) {
        let gradesInfo = `
            <div class="container">
            <table class="table">
                <tr>
                    <th>Id</th>
                    <th>Date</th>
                    <th>Grade</th>
                    <th onclick="postGrade(${student.id})"><button id="addGrade">Add</button></th>
                </tr>
        `
        student.grades.forEach(grade => {
            gradesInfo += `
                <tr>
                    <td>${grade.id}</td>
                    <td>${grade.date}</td>
                    <td>${grade.grade}</td>
                    <td onclick="deleteGrade(${grade.id})"><button id="deleteBtn">Delete</button></td>
                    <td onclick="putGrade(${grade.id})"><button id="editBtn">Edit</button></td>
                </tr>
            `
        })
        gradesInfo += '</table></div>'
        studentInfo += gradesInfo

        if (Array.isArray(student.lectures)) {
                let lecturesInfo = `
                    <div class="container">
                    <table class="table">
                        <tr>
                            <th>Studies</th>
                            <th onclick="postLecture(${student.id})"><button id="addLec">Add lecture</button></th>
                        </tr>
                `
                student.lectures.forEach(lecture => {
                    lecturesInfo += `
                        <tr>
                            <td>${lecture.name}</td>
                            <td onclick="deleteLecture(${lecture.id})"><button id="deleteBtn">Delete</button></td>
                            <td onclick="putLecture(${lecture.id})"><button id="editBtn">Edit</button></td>
                        </tr>
                    `
                })
                lecturesInfo += '</table><button id="btn">Back</button></div>'
                studentInfo += lecturesInfo
    }

    let container = document.getElementById("container")
 
    container.innerHTML = studentInfo
    backButton()
}
}

function backButton() {
    let backButton = document.getElementById("btn");
    backButton.addEventListener("click", function(){
      window.location = "/index.html";
    });
}

function addStudent() {
        editTable()
        let submitBtn = document.getElementById('submitBtn')
        submitBtn.addEventListener('click', function() {
             var xhr = new XMLHttpRequest();
                    xhr.open('POST', '/studentai', true);
                    xhr.setRequestHeader('Content-Type', 'application/json');
                    xhr.send(JSON.stringify({
                        firstName: document.getElementById("fName").value,
                        lastName: document.getElementById("lName").value,
                        email: document.getElementById("email").value
                    }))
                     loadStudent(id)
             })
}




function deleteStudent(id) {
 var xhr = new XMLHttpRequest();
 xhr.open("DELETE", 'studentai/' + id, true);
 xhr.onload = function () {
 	var users = JSON.parse(xhr.responseText);
 	if (xhr.readyState == 4 && xhr.status == "200") {
 		console.table(users);
 	} else {
 		console.error(users);
 	}
 }
 xhr.send();
 loadStudents(100, 1);
}

function editStudent(id) {
     editTable()
      let submitBtn = document.getElementById('submitBtn')
     submitBtn.addEventListener('click', function() {
                   var xhr = new XMLHttpRequest();
                                   xhr.open('PUT', '/studentai/' + id, true);
                                   xhr.setRequestHeader('Content-Type', 'application/json');
                                   xhr.send(JSON.stringify({
                                       firstName: document.getElementById("fName").value,
                                       lastName: document.getElementById("lName").value,
                                       email: document.getElementById("email").value
                                   }))

                                      window.location = "/index.html";
                                   })
}

function editTable(){
    let form = document.getElementById('forma')
    let table = `<form>
                 First name:<br>
                 <input type="text" name="firstname" id="fName" value="">
                 <br>
                 Last name:<br>
                 <input type="text" name="lastname" id="lName" value="">
                 <br>
                 Email:<br>
                 <input type="text" name="email" id="email" value=""><br>
                 <input type="submit" id="submitBtn" value="Add student">
             </form>`
     form.innerHTML = table
}

function addGradeForm() {
     let form = document.getElementById('forma')
        let table = `<form>
                     Date:<br>
                     <input type="text" name="date" id="date" value="">
                     <br>
                     Grade:<br>
                     <input type="text" name="grade" id="grade" value="">
                     <br>
                     <input type="submit" id="submitGradeBtn" value="Add Grade">
                 </form>`
         form.innerHTML = table
}

function postGrade(id) {
     addGradeForm()
         let submitBtn = document.getElementById('submitGradeBtn')
         submitBtn.addEventListener('click', function() {
                       var xhr = new XMLHttpRequest();
                                       xhr.open('POST', '/studentai/' + id, true);
                                       xhr.setRequestHeader('Content-Type', 'application/json');
                                       xhr.send(JSON.stringify({
                                           date: document.getElementById("date").value,
                                           grade: document.getElementById("grade").value
                                       }))
                                       loadStudent(id)
                                       })
}

function putGrade(id) {
     addGradeForm()
         let submitBtn = document.getElementById('submitGradeBtn')
         submitBtn.addEventListener('click', function() {
                       var xhr = new XMLHttpRequest();
                                       xhr.open('PUT', '/studentai/grade/' + id, true);
                                       xhr.setRequestHeader('Content-Type', 'application/json');
                                       xhr.send(JSON.stringify({
                                           date: document.getElementById("date").value,
                                           grade: document.getElementById("grade").value
                                       }))
                                       loadStudent(id)
                                       })
}

function deleteGrade(id) {
 var xhr = new XMLHttpRequest();
 xhr.open("DELETE", 'studentai/grade/' + id, true);
 xhr.onload = function () {
 	var users = JSON.parse(xhr.responseText);
 	if (xhr.readyState == 4 && xhr.status == "200") {
 		console.table(users);
 	} else {
 		console.error(users);
 	}
 }
 xhr.send();
}

function addLectureForm() {
     let form = document.getElementById('forma')
        let table = `<form>
                     Lecture:<br>
                     <input type="text" name="lecture" id="lecture" value="">
                     <br>
                     <input type="submit" id="submitLectureBtn" value="Add Lecture">
                 </form>`
         form.innerHTML = table
}

function postLecture(id) {
     addLectureForm()
         let submitBtn = document.getElementById('submitLectureBtn')
         submitBtn.addEventListener('click', function() {
                       var xhr = new XMLHttpRequest();
                                       xhr.open('POST', '/studentai/lecture/' + id, true);
                                       xhr.setRequestHeader('Content-Type', 'application/json');
                                       xhr.send(JSON.stringify({
                                           name: document.getElementById("lecture").value
                                       }))
                                       window.location.reload(true);
                                       loadStudent(id)
                                       })
}

function putLecture(id) {
     addLectureForm()
         let submitBtn = document.getElementById('submitLectureBtn')
         submitBtn.addEventListener('click', function() {
                       var xhr = new XMLHttpRequest();
                                       xhr.open('PUT', '/studentai/lecture/' + id, true);
                                       xhr.setRequestHeader('Content-Type', 'application/json');
                                       xhr.send(JSON.stringify({
                                           name: document.getElementById("lecture").value
                                       }))
                                       reload()
                                       showStudent(student)
                                       })
}

function deleteLecture(id) {
 var xhr = new XMLHttpRequest();
 xhr.open("DELETE", 'studentai/lecture/' + id, true);
 xhr.onload = function () {
 	var users = JSON.parse(xhr.responseText);
 	if (xhr.readyState == 4 && xhr.status == "200") {
 		console.table(users);
 	} else {
 		console.error(users);
 	}
 }
 xhr.send();
}












