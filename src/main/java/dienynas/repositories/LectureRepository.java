package dienynas.repositories;

import dienynas.entities.Lecture;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface LectureRepository extends PagingAndSortingRepository<Lecture, Integer> {
}
