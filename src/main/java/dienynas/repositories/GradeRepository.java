package dienynas.repositories;

import dienynas.entities.Grade;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface GradeRepository extends PagingAndSortingRepository<Grade, Integer> {
}
