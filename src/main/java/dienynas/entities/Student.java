package dienynas.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import dienynas.entities.Grade;
@Entity
@Table(name = "studentai")
public class  Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "vardas")
    private String firstName;

    @Column(name = "pavarde")
    private String lastName;

    @Column(name = "el_pastas")
    private String email;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "student")
    private List<Grade> grades;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "dalykai_studentai",
            joinColumns = @JoinColumn(name = "studentas_id"),
            inverseJoinColumns = @JoinColumn(name = "dalykas_id"))
    private Set<Lecture> lectures = new HashSet<>();

    public Set<Lecture> getLectures() {
        return lectures;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Grade> getGrades() {
        return grades;
    }

    public void setGrades(List<Grade> grades) {
        this.grades = grades;
    }

    public void addTag(Lecture lecture) {
        lectures.add(lecture);
        lecture.getStudents().add(this);
    }

    public void removeTag(Lecture lecture) {
        lectures.remove(lecture);
        lecture.getStudents().remove(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        return id != null && id.equals(((Student) o).getId());
    }

    @Override
    public int hashCode() {
        return 31;
    }
}
