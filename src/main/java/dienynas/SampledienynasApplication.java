package dienynas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampledienynasApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampledienynasApplication.class, args);
	}

}
