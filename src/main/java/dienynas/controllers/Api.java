package dienynas.controllers;

import dienynas.entities.Grade;
import dienynas.entities.Lecture;
import dienynas.entities.Student;
import dienynas.repositories.GradeRepository;
import dienynas.repositories.LectureRepository;
import dienynas.repositories.StudentRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
//@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
//@EnableAutoConfiguration(exclude = {
//        SecurityAutoConfiguration.class,
//        ManagementSecurityAutoConfiguration.class})
@RestController
@RequestMapping("/studentai")
public class Api {

    private final StudentRepository studentRepository;
    private final GradeRepository gradeRepository;
    private final LectureRepository lectureRepository;

    public Api(StudentRepository studentRepository, GradeRepository gradeRepository, LectureRepository lectureRepository) {
        this.studentRepository = studentRepository;
        this.gradeRepository = gradeRepository;
        this.lectureRepository = lectureRepository;
    }

    @PermitAll
    @GetMapping
    public ResponseEntity<List<Student>> getStudentsPage(
            @RequestParam(required = false, defaultValue = "100") int pageSize,
            @RequestParam(required = false, defaultValue = "1") int pageNo,
            @RequestParam(required = false) String orderBy,
            @RequestParam(required = false, defaultValue = "true") Boolean asc) {

        Sort sort = orderBy == null ? Sort.unsorted() :
                Sort.by(asc == null || asc
                        ? Sort.Order.asc(orderBy)
                        : Sort.Order.desc(orderBy));

        Page<Student> page = studentRepository.findAll(
                PageRequest.of(pageNo - 1, pageSize, sort));
        List<Student> students =  page.toList();

        return ResponseEntity.ok(students);
    }

    @RolesAllowed({"user", "admin"})
    @GetMapping("/{id}")
    public ResponseEntity<Student> getStudentById(@PathVariable int id) {
        Optional<Student> student = studentRepository.findById(id);
        return student.isPresent() ?
                ResponseEntity.ok(student.get()) :
                ResponseEntity.notFound().build();
    }

    @RolesAllowed({"admin"})
    @PostMapping
    public ResponseEntity<Student> create(@RequestBody Student student) {
        return ResponseEntity.ok(studentRepository.save(student));
    }

    @PermitAll
    @DeleteMapping("/{id}")
    ResponseEntity<Integer> delete(@PathVariable int id) {
        Optional<Student> studentOpt = studentRepository.findById(id);
        if (!studentOpt.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        Student student = studentOpt.get();
        studentRepository.delete(student);
        return ResponseEntity.ok(id);
    }

    @PutMapping("/{id}")
    @RolesAllowed({"admin"})
    public ResponseEntity<Student> edit(@PathVariable int id, @RequestBody Student student) {
        Optional<Student> studentDb = studentRepository.findById(id);
        if (!studentDb.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        Student studentUpd = studentDb.get();

        studentUpd.setEmail(student.getEmail());
        studentUpd.setFirstName(student.getFirstName());
        studentUpd.setLastName(student.getLastName());

        return ResponseEntity.ok(studentRepository.save(studentUpd));
    }

    @RolesAllowed({"admin"})
    @PostMapping("/{id}")
    ResponseEntity<Student> addGrade(@PathVariable int id, @RequestBody Grade grade) {
        Optional<Student> studentOpt = studentRepository.findById(id);
        if (!studentOpt.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        Student student = studentOpt.get();
        if (student.getGrades() == null) student.setGrades(new ArrayList<>());

        grade.setStudent(student);
        student.getGrades().add(grade);
        return ResponseEntity.ok(studentRepository.save(student));
    }

    @RolesAllowed({"admin"})
    @DeleteMapping("/grade/{id}")
    ResponseEntity<Integer> deleteGrade(@PathVariable int id) {
        Optional<Grade> optionalGrade = gradeRepository.findById(id);
        if (!optionalGrade.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        Grade grade = optionalGrade.get();
        gradeRepository.delete(grade);
        return ResponseEntity.ok(id);
    }

    @RolesAllowed({"admin"})
    @PutMapping("/grade/{id}")
    public ResponseEntity<Grade> editGrade(@PathVariable int id, @RequestBody Grade grade) {
        Optional<Grade> optionalGrade = gradeRepository.findById(id);
        if (!optionalGrade.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        Grade gradeUpd = optionalGrade.get();

        gradeUpd.setDate(grade.getDate());
        gradeUpd.setGrade(grade.getGrade());

        return ResponseEntity.ok(gradeRepository.save(gradeUpd));
    }



    @RolesAllowed({"admin"})
    @PostMapping("/lecture/{id}")
    ResponseEntity<Student> addLecture(@PathVariable int id, @RequestBody Lecture lecture) {
        Optional<Student> studentOpt = studentRepository.findById(id);
        if (!studentOpt.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        Student student = studentOpt.get();
        student.getLectures().add(lecture);
        return ResponseEntity.ok(studentRepository.save(student));
    }

    @RolesAllowed({"admin"})
    @DeleteMapping("/lecture/{id}")
    ResponseEntity<Integer> deleteLecture(@PathVariable int id) {
        Optional<Lecture> lectureOptional = lectureRepository.findById(id);
        if (!lectureOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        Lecture lecture = lectureOptional.get();
        lectureRepository.delete(lecture);
        return ResponseEntity.ok(id);
    }

    @RolesAllowed({"admin"})
    @PutMapping("/lecture/{id}")
    public ResponseEntity<Lecture> editGrade(@PathVariable int id, @RequestBody Lecture lecture) {
        Optional<Lecture> lectureOptional = lectureRepository.findById(id);
        if (!lectureOptional.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        Lecture lectureUpd = lectureOptional.get();

        lectureUpd.setName(lecture.getName());

        return ResponseEntity.ok(lectureRepository.save(lectureUpd));
    }
}
